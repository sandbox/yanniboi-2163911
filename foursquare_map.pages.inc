<?php

/**
 * @file
 * Contains page and form callbacks for the Foursquare Map module.
 */

/**
 * Page callback for Foursquare admin settings form.
 */
function foursquare_map_admin_form($form, &$form_state) {
  $form['oauth'] = array(
    '#type' => 'fieldset',
    '#title' => t('OAuth Settings'),
    '#access' => module_exists('oauth_common'),
    '#description' => t('To enable OAuth based access for foursquare, you must <a href="@url" target="blank_">register your application</a> with Foursquare and add the provided keys here.', array('@url' => 'https://foursquare.com/developers/register')),
  );
  $form['oauth']['callback_url'] = array(
    '#type' => 'item',
    '#title' => t('Callback URL'),
    '#markup' => url('foursquare_map/oauth', array('absolute' => TRUE)),
  );
  $form['oauth']['foursquare_map_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#default_value' => variable_get('foursquare_map_client_id', NULL),
  );
  $form['oauth']['foursquare_map_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret'),
    '#default_value' => variable_get('foursquare_map_client_secret', NULL),
  );
  $form['oauth']['foursquare_map_access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Access Token'),
    '#description' => t('Your access token will be added automatically when you
      add authenticate your Foursquare account.'),
    '#disabled' => TRUE,
    '#default_value' => variable_get('foursquare_map_access_token', NULL),
  );

  return system_settings_form($form);
}

/**
 * Page callback for Foursquare account form.
 */
function foursquare_map_account_form($form, &$form_state) {

  $form['register_app'] = array(
    '#type' => 'fieldset',
    '#title' => t('Register Foursquare Application'),
    '#weight' => 4,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['add_account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add Foursquare account'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

    if (TRUE) {
      $form['register_app']['form'] = drupal_get_form('foursquare_map_admin_form');
    }

    //if (user_access('')) {
    if (TRUE) {
      $form['add_account']['form'] = drupal_get_form('foursquare_map_add_account_form');
    }

  return $form;
}

/**
 * Form to add a Foursquare account.
 */
function foursquare_map_add_account_form($form, $form_state) {
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go to Foursquare to authenticate an account'),
  );

  return $form;
}

/**
 * Form validation for adding a new Foursquare account.
 */
function foursquare_map_add_account_form_validate($form, &$form_state) {
  $key = variable_get('foursquare_map_client_id', '');
  $secret = variable_get('foursquare_map_client_secret', '');
  if ($key == '' || $secret == '') {
    form_set_error('', t('Please configure your consumer key and secret key at ' .
      '<a href="!url">Foursquare settings</a>.', array( '!url' => url('admin/config/services/foursquare_map'),
    )));
  }
}

/**
 * Form submit handler for adding a Foursquare account.
 */
function foursquare_map_add_account_form_submit($form, &$form_state) {
  $key = variable_get('foursquare_map_client_id', '');

  drupal_goto('https://foursquare.com/oauth2/authenticate', array(
    'query' => array(
      'client_id' => $key,
      'response_type' => 'code',
      'redirect_uri' => url('foursquare_map/oauth', array('absolute' => TRUE)),
    )
  ));
}

/**
 * Page callback for Foursquare OAuth registration.
 */
function foursquare_map_oauth_callback($form, &$form_state) {
  if (empty($_GET['code'])) {
    drupal_set_message(t('The connection to Foursquare failed. Please try again.'), 'error');
    global $user;
    if ($user->uid) {
      // User is logged in, was attempting to OAuth a Foursquare account.
      drupal_goto('admin/config/services/foursquare_map');
    }
    else {
      // Anonymous user, redirect to front page.
      drupal_goto('<front>');
    }
  }
  $form_state['values']['code'] = $_GET['code'];
  drupal_form_submit('foursquare_map_oauth_callback_form', $form_state);
}

/**
 * Form to process the Foursquare OAuth request.
 */
function foursquare_map_oauth_callback_form($form, &$form_state) {
  $form['#post']['code'] = $_GET['code'];
  $form['code'] = array(
    '#type' => 'hidden',
    '#default_value' => $_GET['code'],
  );
  return $form;
}

/**
 * Validate results from Foursquare OAuth return request.
 */
function foursquare_map_oauth_callback_form_validate($form, &$form_state) {
  $key = variable_get('foursquare_map_client_id', '');
  $secret = variable_get('foursquare_map_client_secret', '');

  if ($foursquare = new Foursquare($key, $secret, $form_state['values']['code'])) {
    // Collect oauth_verifier from url
    if ($response = $foursquare->get_request_token()) {
      $form_state['response'] = json_decode(reset(array_keys($response)));
    }
    else {
      form_set_error('code', t('Invalid Foursquare OAuth request'));
    }
  }
  else {
    form_set_error('code', t('Invalid Foursquare OAuth request'));
  }
}

/**
 * Submit results from Foursquare OAuth return request.
 */
function foursquare_map_oauth_callback_form_submit($form, &$form_state) {
  if (!isset($form_state['response']->access_token)) {
    return;
  }

  variable_set('foursquare_map_access_token', $form_state['response']->access_token);
  drupal_set_message(t('Foursquare account successfully validated.'), 'status');
  drupal_goto('admin/config/services/foursquare_map');
}

