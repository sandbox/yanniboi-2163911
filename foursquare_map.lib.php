<?php

/**
 * @file
 * Foursquare class to integrate with the Foursquare API.
 */

/**
 * Primary Foursquare API implementation class
 */
class Foursquare {
  /**
   * @var OAuthConsumer $client built using Client ID and Client Secret
   */
  protected $client;

  /**
   * @var OAuthConsumer $code
   */
  protected $code;

  protected $signature_method;

  /**
   * Constructor for the Foursquare class
   */
  public function __construct($client_key, $client_secret, $code = NULL) {
    $this->client = new OAuthConsumer($client_key, $client_secret);
    $this->signature_method = new OAuthSignatureMethod_HMAC_SHA1();
    if (!empty($code)) {
      $this->code = $code;
    }
  }

  /**
   * Gets the access token by oauth request.
   */
  public function get_request_token() {
    $url = 'https://foursquare.com/oauth2/access_token';
    try {
      $params = array(
        'redirect_uri' => url('foursquare_map/oauth', array('absolute' => TRUE)),
        'client_id' => $this->client->key,
        'client_secret' => $this->client->secret,
        'grant_type' => 'authorization_code',
        'code' => $this->code,
      );
      $response = $this->auth_request($url, $params);
    }
    catch (Exception $e) {
      watchdog('foursquare_map', '!message', array('!message' => $e->__toString()), WATCHDOG_ERROR);
      return FALSE;
    }
    parse_str($response, $token);
    return $token;
  }

  /**
   * Build a request.
   */
  public function checkin_request($access_token, $limit) {
    $url = 'https://api.foursquare.com/v2/users/self/checkins';
    $params = array(
      'oauth_token' => $access_token,
      'v' => 20131227,
      'limit' => $limit,
    );

    return $this->auth_request($url, $params, 'GET');
  }

  /**
   * Performs an authenticated request.
   */
  public function auth_request($url, $params = array(), $method = 'GET') {
    $request = new OAuthRequest($method, $url, $params);
    switch ($method) {
      case 'GET':
        return $this->request($request->to_url());
      case 'POST':
        return $this->request($request->get_normalized_http_url(), $request->get_parameters(), 'POST');
    }
  }

  /**
   * Performs a request.
   *
   * @throws Exception
   */
  protected function request($url, $params = array(), $method = 'GET') {
    $data = '';
    if (count($params) > 0) {
      if ($method == 'GET') {
        $url .= '?'. http_build_query($params, '', '&');
      }
      else {
        $data = http_build_query($params, '', '&');
      }
    }

    $headers = array();

    $headers['Authorization'] = 'Oauth';
    $headers['Content-type'] = 'application/x-www-form-urlencoded';

    $response = $this->doRequest($url, $headers, $method, $data);
    if (!isset($response->error)) {
      return $response->data;
    }
    else {
      $error = $response->error;
      $data = $this->parse_response($response->data);
      if (isset($data['error'])) {
        $error = $data['error'];
      }
      throw new Exception($error);
    }
  }

  /**
   * Actually performs a request.
   *
   * This method can be easily overriden through inheritance.
   *
   * @param string $url
   *   The url of the endpoint.
   * @param array $headers
   *   Array of headers.
   * @param string $method
   *   The HTTP method to use (normally POST or GET).
   * @param array $data
   *   An array of parameters
   * @return
   *   stdClass response object.
   */
  protected function doRequest($url, $headers, $method, $data) {
    return drupal_http_request($url, array('headers' => $headers, 'method' => $method, 'data' => $data));
  }

  protected function parse_response($response) {
    // http://drupal.org/node/985544 - json_decode large integer issue
    $length = strlen(PHP_INT_MAX);
    $response = preg_replace('/"(id|in_reply_to_status_id)":(\d{' . $length . ',})/', '"\1":"\2"', $response);
    return json_decode($response, TRUE);
  }

  /**
   * Get an array of FoursquareCheckin objects.
   */
  public function get_checkins($response) {
    // Check on successfull call
    if ($response) {
      $checkins = array();
      foreach ($response->checkins->items as $checkin) {
        if ($checkin->photos->count == 0) {
          $checkins[] = new FoursquareCheckin($checkin);
        }
        else {
          $checkins[] = new FoursquarePhotoCheckin($checkin);
        }
      }
      return $checkins;
    }
    else {
      return array();
    }
  }

  /**
   * Get an array of FoursquareCheckin objects.
   */
  public function get_checkins_single_photo($response, $image_size = 100) {
    // Check on successfull call
    if ($response) {
      $checkins = array();
      foreach ($response->checkins->items as $checkin) {
        if ($checkin->photos->count == 0) {
          $checkins[] = new FoursquareCheckin($checkin);
        }
        else {
          $checkins[] = new FoursquarePhotoCheckin($checkin, $image_size, TRUE);
        }
      }
      return $checkins;
    }
    else {
      return array();
    }
  }
}

/**
 * Foursquare Checkin class
 */
class FoursquareCheckin {

  protected $id;

  public $created;

  public $type;

  public $shout;

  public $venue;

  /**
   * Constructor for the FoursquareCheckin class
   */
  public function __construct($checkin) {
    $this->id = $checkin->id;
    $this->created = $checkin->createdAt;
    $this->type = 'non_photo';
    if (!empty($checkin->shout)) {
      $this->shout = $checkin->shout;
    }
    else {
      $this->shout = '';
    }
    $this->venue = new FoursquareVenue($checkin->venue);
  }

}

/**
 * Foursquare Checkin class for Photo Checkins
 */
class FoursquarePhotoCheckin extends FoursquareCheckin {

  public $photos;

  /**
   * Constructor for the FoursquarePhotoCheckin class
   */
  public function __construct($checkin, $image_size, $single = FALSE) {
    parent::__construct($checkin);

    $this->type = 'photo';
    $this->photos = $this->get_photos($checkin->photos, $image_size, $single);
  }

  /**
   * Gets photos form a Foursqaure Checkin
   */
  protected function get_photos($photos, $image_size, $single) {
    if ($single) {
      $photo = reset($photos->items);
      return new FoursquarePhoto($photo, $image_size);
    }

    $values = array();

    foreach ($photos->items as $photo) {
      $values[] = new FoursquarePhoto($photo, $image_size);
    }

    return $values;
  }
}

/**
 * Foursquare Venue class
 */
class FoursquareVenue {

  protected $id;

  public $name;

  public $location;

  /**
   * Constructor for the Foursquare Venue class
   */
  public function __construct($venue) {
    $this->id = $venue->id;
    $this->name = $venue->name;
    $this->location = $venue->location;
  }
}

/**
 * Foursquare Photo class
 */
class FoursquarePhoto {

  protected $id;

  public $created;

  public $source;

  public $location;

  public $max_resolution;

  /**
   * Constructor for the Foursquare Photo class
   */
  public function __construct($photo, $image_size) {
    $this->id = $photo->id;
    $this->created = $photo->createdAt;
    $this->source = $photo->prefix . 'width' . $image_size . $photo->suffix;
    $this->max_resolution = $photo->width;
  }
}
