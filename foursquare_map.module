<?php

/**
 * @file
 * The module file for Foursquare Map module.
 */


/**
 * Implements hook_menu().
 */
function foursquare_map_menu() {
  $items['foursquare_map/oauth'] = array(
    'title' => 'Foursquare OAuth',
    'access callback' => TRUE,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('foursquare_map_oauth_callback'),
    'type' => MENU_CALLBACK,
    'file' => 'foursquare_map.pages.inc',
  );

  $items['admin/config/services/foursquare_map'] = array(
    'title' => t('Foursquare Map'),
    'access callback' => TRUE, // @TODO Make a permission.
    'page callback' => 'drupal_get_form',
    'page arguments' => array('foursquare_map_account_form'),
    'type' => MENU_CALLBACK,
    'file' => 'foursquare_map.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_block_info().
 *
 * Defines the new instagram block.
 */
function foursquare_map_block_info() {
  $blocks['foursquare_map'] = array(
    'info' => t('Foursquare Map'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );

  return $blocks;
}

/**
 * Implements hook_block_configure().
 *
 * Set values to be used when rendering the block later.
 */
function foursquare_map_block_configure($delta = '') {
  $form = array();

  if ($delta == 'foursquare_map') {
    // Create a keyed array of blank defaults for the storage variable.
    $empty = array(
      'zoom' => 9,
      'limit' => 10,
      'map_height' => 500,
      'popup' => FALSE,
      //'width' => '100',
      //'height' => '100',
      'image_size' => 100,
    );

    // Store data from variable in $form for now.
    $form['#data'] = variable_get('foursquare_map_block_data', $empty);

    $form['zoom'] = array(
      '#type' => 'textfield',
      '#title' => t('Zoom Level'),
      '#description' => t('How far the map is zoomed in. Available from 1 - 19'),
      '#default_value' => isset($form['#data']['zoom']) ? $form['#data']['zoom'] : 9,
    );

    $form['limit'] = array(
      '#type' => 'textfield',
      '#title' => t('Checkin Limit'),
      '#description' => t('Max number of Checkins to display.'),
      '#default_value' => isset($form['#data']['limit']) ? $form['#data']['limit'] : 10,
    );

    $form['map_height'] = array(
      '#type' => 'textfield',
      '#title' => t('Map height'),
      '#description' => t('Numeric height of the block in px.'),
      '#default_value' => isset($form['#data']['map_height']) ? $form['#data']['map_height'] : 500,
    );

    $form['popup'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display images in a popup box.'),
      //'#description' => t('Numeric height of the block in px.'),
      '#default_value' => isset($form['#data']['popup']) ? $form['#data']['popup'] : FALSE,
    );

    $form['image_size'] = array(
      '#type' => 'textfield',
      '#title' => t('Image Size'),
      '#description' => t('Size of images in popup.'),
      '#default_value' => isset($form['#data']['image_size']) ? $form['#data']['image_size'] : 100,
    );
  }

  return $form;
}

/**
 * Implements hook_block_save().
 *
 * Save the information from the configuration form to a drupal variable.
 */
function foursquare_map_block_save($delta = '', $edit = array()) {
  if ($delta == 'foursquare_map') {
    variable_set('foursquare_map_block_data', $edit);
  }
}

/**
 * Implements hook_block_view().
 *
 * Fetches the instagram photos and creates a render array.
 */
function foursquare_map_block_view($delta = '') {
  $block = array();

  if ($delta == 'foursquare_map') {
    // Get variables.
    $client_key = variable_get('foursquare_map_client_id', '');
    $client_secret = variable_get('foursquare_map_client_secret', '');
    $access_token = variable_get('foursquare_map_access_token', '');

    // Get configuration values.
    $data = variable_get('foursquare_map_block_data', array());

    // Check that block configuration is available.
    if (empty($access_token)) {
      // Remind user to fill in configuration.
      $content = foursquare_map_get_configuration_reminder();
    }
    else {
      $content = array('#markup' => '<div id="map-canvas" style="height: ' . $data['map_height'] . 'px;"></div>');
    // Get a Foursquare connection object.
    $foursquare = new Foursquare($client_key, $client_secret);

    // Request recent checkins from Foursquare.
    $request = $foursquare->checkin_request($access_token, $data['limit']);
    $response = json_decode($request);

    $checkins = $foursquare->get_checkins_single_photo($response->response, $data['image_size']);

    // Add instagram_block specific styles.
    //drupal_add_css(drupal_get_path('module', 'foursquare_map') . '/foursquare-map.css');
    drupal_add_js(array('foursquareMapZoom' => array('value' => $data['zoom'])), 'setting');
    drupal_add_js(array('foursquareMapLat' => array('value' => 50)), 'setting');
    drupal_add_js(array('foursquareMapLng' => array('value' => 10)), 'setting');
    drupal_add_js(array('foursquareMapMarkers' => array('markers' => $checkins)), 'setting');
    drupal_add_library('googleapilibs', 'google_maps');
    drupal_add_js(drupal_get_path('module', 'foursquare_map') . '/foursquare-map.js');

    }


    $block['subject'] = 'Foursquare Map';
    $block['content'] = $content;
  }

  return $block;
}

/**
 * Message to replace block if no Foursquare account has been verified.
 */
function foursquare_map_get_configuration_reminder() {
  $modules = system_rebuild_module_data();
  $configure = $modules['foursquare_map']->info['configure'];
  $url = l('configure', $configure);

  $output = 'Before using this block please ';
  $output .= $url;
  $output .= ' Foursquare Map.';

  return array(
    '#markup' => t($output),
  );
}
