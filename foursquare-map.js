function initialize(zoom, position) {
  // Position for the most recent checkin.
  var mostRecent = new google.maps.LatLng(position.lat, position.lng);

  // Map position and zoom settings.
  var mapOptions = {
    center: mostRecent,
    zoom: zoom,
  };

  // Initial Map object.
  var map = new google.maps.Map(document.getElementById("map-canvas"),
    mapOptions);

  return map;
}

function add_marker(map, marker_wrapper) {
  var location = marker_wrapper.venue.location;

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(location.lat, location.lng),
    //draggable:true,
    animation: google.maps.Animation.DROP,
    map: map,
    title: String(marker_wrapper.venue.name)
  });

  if (String(marker_wrapper.type) == "photo") {

    var contentString = '<div id="content">'+
      '<h1 id="firstHeading" class="firstHeading">' + marker_wrapper.venue.name + '</h1>'+
      '<div id="bodyContent">'+
      '<a href="' + marker_wrapper.photos.source + '" />'+
      '<img src="' + marker_wrapper.photos.source + '" />'+
      '</a>'+
      '<p>' + marker_wrapper.shout + '</p>'+
      '</div>'+
      '</div>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 200,
      ShadowStyle: 1,
      Padding: 10,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: "#ccc",
      backgroundColor: '#fff',
      minWidth: 300,
      maxWidth: 300,
      minHeight: 200,
      maxHeight: 200,
      arrowSize: 10,
      arrowPosition: 50,
      arrowStyle: 0
    });

    // Open popup when marker is clicked.
    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map, marker);
    });

    // Close popup when map is clicked.
    google.maps.event.addListener(map, 'click', function() {
      infowindow.close();
    });

    // Mark the picture popups as blue.
    marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
  }
}

Drupal.behaviors.foursquareMapLoad = {
  attach: function (context, settings) {

    var zoom = Number(settings.foursquareMapZoom.value);
    var markers = settings.foursquareMapMarkers.markers;
    var position = markers[0].venue.location;

    var map = initialize(zoom, position);

    for (var i = 0; i < markers.length; i++) {
      var marker = markers[i];
      add_marker(map, marker);
    }
  }
}
